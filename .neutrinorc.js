const airbnb = require('@neutrinojs/airbnb');
const react = require('@neutrinojs/react');
const jest = require('@neutrinojs/jest');

module.exports = {
  options: {
    root: __dirname,
  },
  use: [
    airbnb(),
    react({
      html: {
        title: '4lifeza',
      },
    }),
    jest(),
    (neutrino) => {
      // neutrino.config.plugin('manifest').use(ManifestPlugin, [
      //   {
      //     seed: JSON.parse(fs.readFileSync('./manifest.json')),
      //   },
      // ]);
      // neutrino.config.output.filename('[name].[hash].js');
      // Add PWA tp the production build cycle
      // if (process.env.ENVIRONMENT == 'prod') {
      //   neutrino.use('@neutrinojs/pwa', {
      //     relativePaths: true,
      //     pluginId: 'pwa',
      //   });
      // }
      // neutrino.config.module.rule('_redirect');

      neutrino.config.module
        .rule('redirect')
        .test(/\_redirects$/)
        .use('file')
        .loader('file-loader')
        .options({ name: '_redirects' });

      neutrino.config.module
        .rule('pdf')
        .test(/\.(pdf)$/)
        .use('file')
        .loader('file-loader');

    },
  ],
};