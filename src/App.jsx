import { hot } from 'react-hot-loader';
import React from 'react';
import { Switch, Route, useLocation } from 'react-router-dom';
import {
  TransitionGroup,
  // CSSTransition,
} from 'react-transition-group';

import './App.css';
import { Helmet } from 'react-helmet';
import SideNav from './components/SideNav';
import Home from './components/Home';
import Session from './components/Session';
import Contact from './components/Contact';
import favicon from '../public/favicon.ico';
import Study from './components/Study';
import Footer from './components/Footer';

// eslint-disable-next-line no-unused-vars, import/extensions
import redirect from '../_redirects';
import ViewPDF from './components/ViewPDF';
import ViewPDFStressPain from './components/ViewPDFStressPain';
import SuperFoodPromotion from './components/SuperFoodPromotion';

const App = () => {
  const location = useLocation();

  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>4LifeZA</title>
        <meta
          name="description"
          content="Book Your Kinesiology Session at 4lifeZA."
        />
        <meta
          name="google-site-verification"
          content="SuKnoLTFHDsQWjee7QkJ6Fho-KSr7slkts_DPBRiczo"
        />
        <link
          rel="icon"
          type="image/png"
          href={favicon}
          sizes="16x16"
        />
        <link rel="canonical" href="http://4lifeza.co.za" />
      </Helmet>
      <div className="sidenavContainer">
        <SideNav />
      </div>
      <TransitionGroup>
        <Switch location={location}>
          <Route path="/about">
            <div
              style={{
                backgroundColor: 'black',
                height: '100vh',
              }}
            >
              {/* ABOUT */}
            </div>
          </Route>

          <Route path="/contact">
            <Contact />
          </Route>

          <Route path="/sessions">
            <Session />
          </Route>

          <Route path="/study">
            <Study />
          </Route>

          <Route path="/kpower-epiginetics">
            <ViewPDF />
          </Route>

          <Route path="/stress-pain-management">
            <ViewPDFStressPain />
          </Route>

          <Route path="/superfood-promotion">
            <SuperFoodPromotion />
          </Route>

          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </TransitionGroup>
      <Footer />
    </>
  );
};

export default hot(module)(App);
