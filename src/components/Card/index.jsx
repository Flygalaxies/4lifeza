import { hot } from 'react-hot-loader';
import React from 'react';
import * as PropTypes from 'prop-types';

import './styles.css';

const Card = ({ title, children }) => (
  <div className="cardContainer">
    <h1 className="cardTitle">{title}</h1>
    <div className="cardContent">{children}</div>
  </div>
);

Card.propTypes = {
  title: PropTypes.string,
  children: PropTypes.element,
};

Card.defaultProps = {
  title: 'Default Title',
  children: <></>,
};

export default hot(module)(Card);
