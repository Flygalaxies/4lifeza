import { hot } from 'react-hot-loader';
import React from 'react';
import Map from '../Map';

import './styles.css';
import Card from '../Card';

const Contact = () => (
  <div className="contactContainer">
    <div className="contact">
      <div>
        <Card title="Contact Details">
          <a href="mailto: babsie@4lifeza.co.za">
            babsie@4lifeza.co.za
          </a>
        </Card>
        <p>+27 61 795 3604</p>
        <p>152 Charles Road, Bredell A.H, Kempton Park</p>
        <Map />
      </div>
    </div>
  </div>
);

Contact.propTypes = {};

Contact.defaultProps = {};

export default hot(module)(Contact);
