import { hot } from 'react-hot-loader';
import React from 'react';

import './styles.css';

import Card from '../Card';
import Map from '../Map';

import Whatsapp from '../../../public/WhatsAppButtonGreenLarge.png';

const Footer = () => (
  <div className="footerContainer">
    <div className="footer" style={{ width: '50%' }}>
      <Card title="Contact Details">
        <a
          href="mailto:babsie@4lifeza.co.za"
          style={{ textDecoration: 'none', color: 'white' }}
        >
          babsie@4lifeza.co.za
        </a>
        <p>+27 61 795 3604</p>
        <a
          aria-label="Chat on WhatsApp"
          href="https://wa.me/27617953604"
        >
          <img
            alt="Chat on WhatsApp"
            src={Whatsapp}
            className="whatsAppImg"
          />
        </a>
        <p>
          152 Charles Road, Bredell A.H, Kempton Park, South
          Africa
        </p>
      </Card>
    </div>
    <div className="map">
      <Map />
    </div>
  </div>
);

export default hot(module)(Footer);
