import { hot } from 'react-hot-loader';
import React from 'react';
import * as PropTypes from 'prop-types';

import './styles.css';

const Hero = ({ title, description }) => (
  <div className="heroContainer">
    <h1 className="heroTitle">{title}</h1>
    <h3 className="heroDescription">{description}</h3>
  </div>
);

Hero.propTypes = {
  title: PropTypes.string,
  description: PropTypes.element,
};

Hero.defaultProps = {
  title: '',
  description: '',
};

export default hot(module)(Hero);
