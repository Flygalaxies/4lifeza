import { hot } from 'react-hot-loader';
import React from 'react';

import './styles.css';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import KinesiologyMoreInfo from '../KinesiologyMoreInfo';

const Home = () => {
  const learnMoreClick = () => {
    const elmnt = document.getElementById('kinesiologyMoreInfo');
    elmnt.scrollIntoView({ behavior: 'smooth' });
  };

  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>4LifeZA</title>
        <meta
          name="description"
          content="The official home page of 4LifeZA, where you can find out more about kinesiology and  book your kinesiology session"
        />
        <meta
          name="keywords"
          content="4lifeza,4life,forlifeza,kinesiology,sessions,session bookings,kinesiology booking,kinesiology south africa"
        />
        <link rel="canonical" href="http://4lifeza.co.za" />
      </Helmet>
      <div className="home">
        <div className="homeMainImage">
          <></>
        </div>
        <div className="homeInformation">
          <div className="homeHeadings">
            <h1 className="homeTitle">Kinesiology</h1>
            <h5 className="homeDescription">
              The art of healing, goal setting & potential
              problems through muscle testing
            </h5>
          </div>
          <div className="extraInfo">
            <Link className="cta" to="/sessions">
              <div>Book A session</div>
            </Link>
            {/* <Link className="cta" to="/sessions">
              <div>Study</div>
            </Link> */}
          </div>
        </div>
        <button
          type="button"
          className="learnMore"
          onClick={learnMoreClick}
          onKeyDown={learnMoreClick}
        >
          Learn more
        </button>
      </div>
      <div
        id="kinesiologyMoreInfo"
        className="kinesiologyMoreInfo"
      >
        <KinesiologyMoreInfo />
      </div>
    </>
  );
};

export default hot(module)(Home);
