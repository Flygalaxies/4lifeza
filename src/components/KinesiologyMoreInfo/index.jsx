import { hot } from 'react-hot-loader';
import React from 'react';

import './styles.css';
import { Link } from 'react-router-dom';
import Card from '../Card';
import Hero from '../Hero';

import Pdf from '../../../public/ICPKPSyllabus.pdf';

const KinesiologyMoreInfo = () => {
  const heroTitle = 'What is Kinesiology?';
  const heroDescription = `Kinesiology is a communication tool that enables a
  person to assess and upgrade their performance in all
  areas of their life, It is the science of energy
  balancing and is grounded in the study of anatomy and
  physiology.`;

  const onButtonClick = () => {
    // using Java Script method to get PDF file
    fetch(Pdf).then((response) => {
      response.blob().then((blob) => {
        // Creating new object of PDF file
        const fileURL = window.URL.createObjectURL(blob);
        // Setting various property values
        const alink = document.createElement('a');
        alink.href = fileURL;
        alink.download = 'ICPKPSyllabus.pdf';
        alink.click();
      });
    });
  };

  return (
    <div className="kmi-Main">
      <div className="kmi-MainContainer">
        <Hero title={heroTitle} description={heroDescription} />
        <div className="kmi-InShort">
          <h1>In short</h1>
          <h3>
            Kinesiology restores balance to the body and mind
          </h3>
        </div>
        <div className="kmi-cardsContainer">
          <div className="kmi-Card">
            <Card title="Balancing?">
              <h3>
                Kinesiology works with muscles, the body, and its
                own energy, Called muscle Testing. It helps
                resolve all kinds of problems in life,
                unwellness, stress, as well as relieves pain. It
                works well with goal setting and future issues
                which may arise.
              </h3>
            </Card>
          </div>
          <div className="kmi-Card">
            <Card title="Muscle Testing?">
              <h3>
                Muscle testing is a profound method that allows
                the Kinesiologist to “talk to your body”.
                Physical issues, such as pain, muscle or other
                structural impairment as a result of surgery,
                injury or traumatic experiences are dealt with on
                a neurological level.
              </h3>
            </Card>
          </div>
        </div>
        <div className="bookASession">
          <Link className="cta" to="/sessions">
            <div>Book A session</div>
          </Link>
        </div>

        <div className="kmi-cardsContainer">
          <Hero title="Want To Study Kinesiology?" />
          <div className="kmi-Card">
            <Card title="Official ICPKP Syllabys">
              <h3>
                Get the Official Syllabys of the ICPKP to see
                what you&aposll learn
              </h3>
              <div
                className="cta"
                onClick={onButtonClick}
                onKeyPress={onButtonClick}
                role="button"
                tabIndex={0}
              >
                Download PDF
              </div>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
};

export default hot(module)(KinesiologyMoreInfo);
