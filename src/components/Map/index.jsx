import { hot } from 'react-hot-loader';
import React from 'react';

const MapComponent = () => {
  const isMobile = Math.min(window.screen.width, window.screen.height) < 768;

  const width = isMobile ? '150' : '500';
  const height = isMobile ? '250' : '200';

  return (
    <iframe
      width={width}
      height={height}
      id="gmap_canvas"
      src="https://maps.google.com/maps?q=152%20Charles%20road%20Breddel&t=&z=13&ie=UTF8&iwloc=&output=embed"
      frameBorder="0"
      scrolling="no"
      marginHeight="0"
      marginWidth="0"
      title="4lifezaGoogleMaps"
      allowFullScreen
      loading="lazy"
    />
  );
};

export default hot(module)(MapComponent);
