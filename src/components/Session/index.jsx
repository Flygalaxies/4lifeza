import { hot } from 'react-hot-loader';
import React from 'react';
import { Helmet } from 'react-helmet';

import './styles.css';

const Session = () => (
  <div className="sessionContainer">
    <Helmet>
      <meta charSet="utf-8" />
      <title>4LifeZA Book your kinesiology session</title>
      <meta
        name="description"
        content="Book Your Kinesiology Session at 4lifeZA."
      />
      <meta
        name="keywords"
        content="4lifeza,4life,forlifeza,kinesiology,sessions,session bookings,kinesiology booking,kinesiology south africa, kinesiology sa, kinesiologyza"
      />
      <link
        rel="canonical"
        href="http://4lifeza.co.za/session"
      />
    </Helmet>
    <div className="sessionBookingForm">
      <iframe
        title="book a session"
        width="640px"
        height="480px"
        src="https://forms.office.com/Pages/ResponsePage.aspx?id=DQSIkWdsW0yxEjajBLZtrQAAAAAAAAAAAAN__tZAKphUNDA0UjVIVktVSkhCWTRLOUg3OEdOMzRKSi4u&embed=true"
        frameBorder="0"
        marginWidth="0"
        marginHeight="0"
        style={{ height: '100%' }}
        allowFullScreen
        webkitallowfullscreen
        mozallowfullscreen
        msallowfullscreen
      >
        {' '}
      </iframe>
    </div>
  </div>
);

export default hot(module)(Session);
