import { hot } from 'react-hot-loader';
import React from 'react';
import { Link } from 'react-router-dom';
import './sidenav.css';

import Logo from '../../../public/4LifeWhite.png';

const SideNav = () => (
  <div className="sidenav">
    <div className="logoContainer">
      <Link to="/">
        <div className="logo">
          <img src={Logo} alt="4LifeZA Official Logo" />
        </div>
      </Link>
    </div>
    <nav>
      <ul>
        <li className="routerLink">
          <Link to="/">Home</Link>
        </li>
        <li className="routerLink">
          <Link to="/sessions">Book A Session</Link>
        </li>
        <li className="routerLink">
          <Link to="/study">Study Kinesiology</Link>
        </li>
      </ul>
    </nav>
  </div>
);

export default hot(module)(SideNav);
