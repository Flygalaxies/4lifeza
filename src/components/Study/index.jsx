import { hot } from 'react-hot-loader';
import React from 'react';

import './styles.css';
import Card from '../Card';
import Hero from '../Hero';

import Pdf from '../../../public/ICPKPSyllabus.pdf';

const Study = () => {
  const onButtonClick = () => {
    // using Java Script method to get PDF file
    fetch(Pdf).then((response) => {
      response.blob().then((blob) => {
        // Creating new object of PDF file
        const fileURL = window.URL.createObjectURL(blob);
        // Setting various property values
        const alink = document.createElement('a');
        alink.href = fileURL;
        alink.download = 'ICPKPSyllabus.pdf';
        alink.click();
      });
    });
  };

  return (
    <div className="studyContainer">
      <div className="study">
        <div className="kmi-cardsContainer">
          <div className="study-mobile">
            <Hero title="Want To Study Kinesiology?" />
            <div className="study-card">
              <Card title="Official ICPKP Syllabys">
                <h3>
                  Get the Official Syllabys of the ICPKP to see
                  what you&aposll learn
                </h3>
                <div
                  className="cta"
                  onClick={onButtonClick}
                  onKeyPress={onButtonClick}
                  role="button"
                  tabIndex={0}
                >
                  Download PDF
                </div>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default hot(module)(Study);
