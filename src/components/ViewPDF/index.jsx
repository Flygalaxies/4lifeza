import { hot } from 'react-hot-loader';
import React, { useState } from 'react';
// import { PDFViewer } from '@react-pdf/renderer';
// import { Document, Page } from 'react-pdf';

import './styles.css';

import Pdf from '../../../public/AugDNA.pdf';

const ViewPDF = () => {
  const [numPages] = useState();
  // const [pageNumber, setPageNumber] = useState(1);
  // console.log('test');

  // function onDocumentLoadSuccess(numberPages) {
  //   setNumPages(numberPages);
  // }

  return (
    <div className="pdfViewer">
      <a href={Pdf} target="_blank" rel="noreferrer">
        VIEW PDF
      </a>
      {numPages}
      {/* <Document>
        <Page />
      </Document> */}
    </div>
  );
};

export default hot(module)(ViewPDF);
