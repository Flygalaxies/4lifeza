import { hot } from 'react-hot-loader';
import React, { useState } from 'react';

import './styles.css';

import Pdf from '../../../public/StressPainManag.pdf';

const ViewPDFStressPain = () => {
  const [numPages] = useState();

  return (
    <div className="pdfViewer">
      <a href={Pdf} target="_blank" rel="noreferrer">
        VIEW PDF
      </a>
      {numPages}
    </div>
  );
};

export default hot(module)(ViewPDFStressPain);
